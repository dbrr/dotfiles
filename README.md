# Dotfiles

* **vimrc file**
	* Configuration settings to initalise when vim starts
* **i3 config file**
	* Everything to do with the i3 windows manager personal settings
* **makesymlinks.sh**
	* For use on linux systems, creates a system link for the config files 
	
## Vim plugins
* [tpope / vim-sensible](https://github.com/tpope/vim-sensible "vim-sensible")
	* Defaults everyone can agree on
* [tpope / vim-fugitive](https://github.com/tpope/vim-fugitive "vim-fugitive")
	* A Git wrapper so awesome, it should be illegal
* [tpope / vim-surround](https://github.com/tpope/vim-surround "vim-surround")
	* quoting/parenthesizing made simple
* [tpope / vim-pathogen](https://github.com/tpope/vim-pathogen "vim-pathogen")
	* manage your runetimepath
* [shime / vim-livedown](https://github.com/shime/vim-livedown "vim-livedown")
	* vim plugin for livedown
	* also required: [Livedown](https://github.com/shime/livedown "Livedown")
		* Live markdown previews for your favorite editor

----
# Installation Instructions

**LINUX**  
Once cloned, make makesymlinks.sh executeable like so:
```
cd ~/dotfiles
chmod +x makesymlinks.sh
./makesymlinks.sh
```

**WINDOWS**  
Place the .vimrc in its correct location, typically:  
```
C:\\Users\userName\_vimrc
```
